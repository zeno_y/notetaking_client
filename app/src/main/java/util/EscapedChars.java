package util;

import android.support.annotation.NonNull;

/**
 * Created by Zeno Yuki on 4/27/18.
 *
 * Not a POJO! Checks incoming text for any characters that need escaping and escapes. Necessary when
 * passing data to remote.
 *
 * "To escape"/"escaped" character pairs are defined by the programmer. Add {@link #buildEscapePairs()}
 * new {@link Pair} of "to escape"/"escaped" as needed.
 */
public class EscapedChars {
    private Pair[] mPairArray;
    private String mEscapedText;

    /**
     * Private bare constructor. Prevents class being instantiated without params.
     */
    private EscapedChars() {}

    /**
     * Constructor.
     * @param textToEscape is of type {@link String} and is the text to look for characters that need
     *                     escaping.
     */
    public EscapedChars(@NonNull String textToEscape) {
        mPairArray = buildEscapePairs();
        mEscapedText = escapeCharacters(textToEscape);
    }

    /**
     * Escapes characters in given string.
     *
     * @param text is of type {@link String} and is the text to process
     * @return is of type {@link String} and is the parameter {@param text} but with escaped characters
     */
    private String escapeCharacters(@NonNull String text) {
        if(mPairArray != null) {
            for(Pair p : mPairArray) {
                text = text.replace(p.getToEscape().toString(), p.getEscaped().toString());
            }
        }

        return text;
    }

    /**
     * Defines the characters to escape.
     * @return is of type {@link Pair[]} and is a list of "to escape"/"escaped" character pairs.
     * @see Pair
     */
    private Pair[] buildEscapePairs() {
        return new Pair[] {
                new Pair("\\", "\\\\"), //MUST BE FIRST
                new Pair("\"", "\\\"")
        };
    }

    //Setters/Getters
    public String getEscapedText() {return mEscapedText;}

    /**
     * Stores a "to escape"/"escaped" character pairs
     */
    private class Pair {
        private CharSequence mToEscape, mEscaped;

        /**
         * Private bare constructor. Prevents class being instantiated without params.
         */
        private Pair() {}

        /**
         * Constructor.
         * @param toEscape is of type {@link CharSequence} and is the character sequence to escape
         * @param escaped is of type {@link CharSequence} and is the equivalent escaped character sequence
         */
        Pair(CharSequence toEscape, CharSequence escaped) {
            setToEscape(toEscape);
            setEscaped(escaped);
        }

        //Setters/Getters
        CharSequence getToEscape() {return mToEscape;}
        void setToEscape(CharSequence mToEscape) {this.mToEscape = mToEscape;}
        CharSequence getEscaped() {return mEscaped;}
        void setEscaped(CharSequence mEscaped) {this.mEscaped = mEscaped;}
    }
}
