package util;

/**
 * Created by Zeno Yuki on 5/23/18.
 *
 * Stores pair of date ranges (start and end) for ease of
 */
public class DateRange {
    private long mStart, mEnd;

    /**
     * Private bare constructor. Prevents class being instantiated without params.
     */
    private DateRange() {
//        final long c = System.currentTimeMillis();
//        setStart(c);
//        setEnd(c);
    }

    /**
     * Constructor.
     *
     * @param start is of primitive type long and is the start of the date range.
     * @param end is of primitive type long and is the end of the date range.
     */
    public DateRange(long start, long end) {
        setStart(start);
        setEnd(end);
    }

    //Setters and Getters
    public long getStart() {return mStart;}
    public void setStart(long start) {mStart = start;}
    public long getEnd() {return mEnd;}
    public void setEnd(long end) {mEnd = end;}
}
