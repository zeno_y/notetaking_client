package util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeno Yuki on 5/10/18.
 *
 * Not a POJO! Used to retrieve a {@link List<String>} from an input of another String.
 * {@link #SPACER} is what is defined as the separator between the keywords.
 */
public class ParsedKeys {
    private static final String SPACER = " ";
    private String mToParse;

    /**
     * Private bare constructor. Prevents class being instantiated without params.
     */
    private ParsedKeys() {}

    /**
     * Constructor.
     * @param toParse is of type {@link String} and is the string object to parse out into a list.
     */
    public ParsedKeys(String toParse) {setToParse(toParse);}

    /**
     * Parses the string into individual words, placing them into a list as items.
     * @return is of type {@link List<String>} and is a list of words parsed from a string.
     */
    public List<String> getParsedKeys() {
        List<String> parsedKeys = new ArrayList<>();

        //Initializes variables
        int i = getToParse().indexOf(SPACER);
        int iPrev = 0;

        //If there is only one keyword, return said keyword in a list.
        if(i < 0) {
            parsedKeys.add(getToParse());
            return parsedKeys;
        }

        //Runs code once while i > 0
        do {
            //Adds word to list
            if(i - iPrev > 1) {
                final String keyword = getToParse().substring(iPrev > 0 ? iPrev + 1 : 0, i);
                parsedKeys.add(keyword);
            }

            //Updates variables to be used in next loop
            iPrev = i;
            i = getToParse().indexOf(SPACER, iPrev + 1);

        } while(i > 0);

        //Returns list
        return parsedKeys;
    }

    //Setters/Getters
    private void setToParse(String toParse) {mToParse = toParse;}
    private String getToParse() {return mToParse;}
}
