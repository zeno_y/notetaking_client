package volley;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import model.Type;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Retrieves note types from remote using Volley.
 * Implement {@link NoteCallback} to retrieve returned data once it is available.
 */
public class VolleyType implements VolleyConst {
    private static final String CMD = "all-types";
    private static final String URI = ROOT + CMD;

    private static final String ID = "id";
    private static final String TYPE = "type";

    /**
     * Retrieves (requests) all note types and calls NoteCallback interface methods once complete.
     *
     * @param cb is of type {@link NoteCallback} and is the class that will receive returned data from
     *           remote.
     */
    static Request requestTypes(final TypeCallback cb) {
        //Instantiates new request
        return new AuthJsonArrayRequest(URI, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //Cycles through all response JSON objects and converts them to app-readable Type objects
                for(int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        Type type = new Type(obj.getLong(ID), obj.getString(TYPE));
                        cb.onTypeReceived(type);

                    } catch(JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Fires callback method
                cb.onTypeFailure(error.toString());
            }
        });
    }
}
