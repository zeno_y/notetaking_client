package volley;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zenoyuki.notr.Universal;

import data.DataAccess;

/**
 * Created by Zeno Yuki on 5/22/18.
 *
 * Retrieves notes statistics from remote using Volley.
 */
public class VolleyStats implements VolleyConst {
    private static final String CMD_MIN_DATE = "min-date";
    private static final String CMD_MAX_DATE = "max-date";

    private static final String URI_MIN_DATE = ROOT + CMD_MIN_DATE;
    private static final String URI_MAX_DATE = ROOT + CMD_MAX_DATE;

    /**
     * Retrieves (requests) minimum date of all notes currently stored in remote.
     * @param cb is of type {@link StatCallback} used to return request data
     */
    static Request requestMinDate(final StatCallback cb) {
        //Instantiates new request
        return new AuthStringRequest(URI_MIN_DATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    long minDate = Long.parseLong(response);
                    //Returns response data
                    cb.onMinDateReceived(minDate);
                } catch(NumberFormatException e) {
                    cb.onMinDateReceived(0L);
                    cb.onStatFailure(DataAccess.SIDE_MIN, e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cb.onMinDateReceived(0L);
                cb.onStatFailure(DataAccess.SIDE_MIN, error.getMessage());
            }
        });
    }

    /**
     * Retrieves (requests) maximum date of all notes currently stored in remote.
     * @param cb is of type {@link StatCallback} used to return request data.
     */
    static Request requestMaxDate(final StatCallback cb) {
        //Instantiates new request
        return new AuthStringRequest(URI_MAX_DATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    long maxDate = Long.parseLong(response);
                    //Returns response data
                    cb.onMaxDateReceived(maxDate);
                } catch(NumberFormatException e) {
                    cb.onMaxDateReceived(0L);
                    cb.onStatFailure(DataAccess.SIDE_MAX, e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cb.onMaxDateReceived(0L);
                cb.onStatFailure(DataAccess.SIDE_MAX, error.getMessage());
            }
        });
    }
}
