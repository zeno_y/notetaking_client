package volley;

import android.content.res.Resources;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.zenoyuki.notr.R;
import com.zenoyuki.notr.Universal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Note;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Retrieves notes from remote using Volley.
 * Implement {@link NoteCallback} to retrieve returned data once it is available.
 */
public class VolleyNote implements VolleyConst {
    private static final int PAGE_SIZE = 10;
    private static final int TIMEOUT_IN_MS = 5000;

    private static final String CMD_POST = "add-note";
    private static final String CMD_GET_ALL = "all-notes";
    private static final String CMD_GET_BY_PAGE = "notes-in-pages?";
    private static final String CMD_GET_BY_DATE = "notes-between-dates?";
    private static final String CMD_GET_BY_KEYWORDS = "notes-with-keyword?";

    private static final String URI_POST = ROOT + CMD_POST;
    private static final String URI_GET_ALL= ROOT + CMD_GET_ALL;
    private static final String URI_GET_BY_PAGE = ROOT + CMD_GET_BY_PAGE;
    private static final String URI_GET_BY_DATE = ROOT + CMD_GET_BY_DATE;
    private static final String URI_GET_BY_KEYWORDS = ROOT + CMD_GET_BY_KEYWORDS;

    private static final String ID = "id";
    private static final String DATE = "date";
    private static final String NOTE = "note";
    private static final String TYPE_ID = "typeId";

    /**
     * Posts note to remote.
     *
     * @param note is of type {@link String} and is the note to post to remote.
     * @param typeId is of primitive type long and is the type ID of the note to post.
     * @param cb is of type NoteCallback and is used to return received data (posted note with post date).
     */
    static Request post(String note, long typeId, final NoteCallback cb) {
        JsonObjectRequest request = null;
        //Tries to create JSON object containing Note to store in remote while checking for JSONException
        try {
            //Creates JSON object to post.
            final JSONObject jsonBody = new JSONObject(getJSONObjectStr(note, typeId));

            //Instantiates new request
            request = new AuthJsonObjectRequest(
                    Request.Method.POST, URI_POST, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Tries to create Note object from returned JSON object while checking for JSONException
                    try {
                        Note note = new Note(
                                response.getLong(ID), response.getLong(DATE),
                                response.getInt(TYPE_ID), response.getString(NOTE)
                        );

                        cb.onNotePosted(note);
                    } catch(JSONException e) {
                        //Fires callback method
                        cb.onNotesFailure(e.getMessage(), false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Fires callback method
                    cb.onNotesFailure(error.getMessage(), false);
                }
            });

            //Adds request to queue
        } catch(JSONException e) {
            //Fires callback method
            cb.onNotesFailure(e.getMessage(), false);
        }

        return request;
    }

    /**
     * Retrieves (requests) all notes by page.
     *
     * @param isNewList is of primitive type boolean and is true if the request is for page zero.
     * @param page is of primitive type int and is the page number to request.
     * @param cb is of type NoteCallback and is used to return received data.
     * @param res
     */
    static Request requestByPage(boolean isNewList, int page, final NoteCallback cb, Resources res) {
        final String params = res.getString(R.string.pages_uri_params, page, PAGE_SIZE);
        //Calls method that actually creates requests
        return getNotes(isNewList, URI_GET_BY_PAGE + params, cb);
    }

    /**
     * Retrieves (requests) notes that are within a certain date range.
     *
     * @param isNewList is of primitive type boolean and is true if the request is for page zero.
     * @param page is of primitive type int and is the page number to request.
     * @param from is of primitive type long and is the range start of returned notes.
     * @param to is of primitive type long and is the range end of returned notes.
     * @param cb is of type NoteCallback and is used to return received data.
     * @param res
     *
     */
    static Request requestByDateRange(boolean isNewList, int page, long from, long to, final NoteCallback cb, Resources res) {
        final String params = res.getString(R.string.date_range_uri_params, from, to, page, PAGE_SIZE);
        //Calls method that actually creates requests
        return getNotes(isNewList, URI_GET_BY_DATE + params, cb);
    }

    /**
     * Retrieves (requests) notes that contain keywords.
     *
     * @param isNewList is of primitive type boolean and is true if the request is for page zero.
     * @param page is of primitive type int and is the page number to request.
     * @param keywords is of type {@link List<String>} and is the list of keywords to search for
     * @param cb is of type NoteCallback and is used to return received data.
     * @param res
     */
    static Request requestByKeywords(
            boolean isNewList, int page, List<String> keywords, final NoteCallback cb, Resources res) {
        String keyword = keywords.get(0);//TODO:TEMPORARY

        //Builds URI
        StringBuilder params = new StringBuilder();
        params.append(res.getString(R.string.keywords_uri_params, keyword));
        params.append("&");
        params.append(res.getString(R.string.pages_uri_params, page, PAGE_SIZE));

        //Calls method that actually creates requests
        return getNotes(isNewList, URI_GET_BY_KEYWORDS + params, cb);
    }

    static Request deleteNotes(long id) {
        //id can equal DialogDeleteAll.DELETE_ALL
        return null;
    }

    /**
     * Retrieves (requests) notes either in full or within certain parameters, defined in previous
     * public methods.
     *
     * @param isNewList is of primitive type boolean and is true when requesting a new set of notes
     *                  (page zero)
     * @param uri is of type {@link String} and is the URI to use to request notes from
     * @param cb is of type NoteCallback and is used to return received data.
     */
    private static Request getNotes(final boolean isNewList, String uri, final NoteCallback cb) {
        //Instantiates new request
        JsonArrayRequest request = new AuthJsonArrayRequest(uri, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<Note> notesList = new ArrayList<>();

                //Iterates through entire JSON response.
                for(int i = 0; i < response.length(); i++) {
                    //Tries to create a Note object for every JSON object while checking for JSONException
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        Note note = new Note(
                                obj.getLong(ID), obj.getLong(DATE), obj.getInt(TYPE_ID), obj.getString(NOTE)
                        );

                        notesList.add(note);
                    } catch(JSONException e) {
                        //Fires callback method
                        cb.onNotesFailure(e.getMessage(), true);
                    }
                }

                //Fires a certain callback method depending on param isNewList
                if(isNewList) cb.onNotesReceived(notesList);
                else cb.onUpdateReceived(notesList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Fires callback method
                Log.d("error", "error occurred");
                cb.onNotesFailure(error.getMessage(), true);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_IN_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));



        return request;
    }

    /**
     * Constructors JSON string for posting {@link Note} to remote.
     *
     * @param note is of type {@link String} and is the note to post.
     * @param typeId is of primitive type long and is the type ID of the posting note.
     * @return is of type {@link String} and is the JSON string used when posting note to remote.
     */
    private static String getJSONObjectStr(String note, long typeId) {
        return "{\"" + NOTE + "\":\"" + note + "\", \"" + TYPE_ID + "\":" + typeId + "}";
    }
}
