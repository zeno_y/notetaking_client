package volley;

/**
 * Created by Zeno Yuki on 6/12/18.
 */
public interface StatCallback {
    void onMinDateReceived(long date);

    void onMaxDateReceived(long date);

    void onStatFailure(int stat, String errorMsg);
}
