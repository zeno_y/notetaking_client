package volley;

import android.content.Context;
import android.content.res.Resources;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.List;

import util.DateRange;

/**
 * Created by Zeno Yuki on 6/12/18.
 *
 * Acts as an interface to HTTP requests that are carried out by Volley. Is a Singleton.
 */
public class Requester {
    private static Requester sThisInstance;
    private RequestQueue mRequestQueue;

    /**
     * Private constructor that is only called by {@link #getInstance(Context)}. Instantiates
     * Volley's {@link RequestQueue}.
     *
     * @param c is of type {@link Context} and is the context of the calling class.
     */
    private Requester(Context c) {
        mRequestQueue = Volley.newRequestQueue(c);
    }

    /**
     * Retrieves instance of the {@link RequestQueue}.
     *
     * @param c is of type {@link Context} and is the context of the calling class.
     * @return is of this type.
     */
    public static Requester getInstance(Context c) {
        if(sThisInstance == null) sThisInstance = new Requester(c);
        return sThisInstance;
    }

    /**
     * Shadows method {@link VolleyType#requestTypes(TypeCallback)}.
     * @param cb is of type {@link TypeCallback} and is the object that receives a callback from Volley.
     */
    public void requestTypes(TypeCallback cb) {addToRequestQueue(VolleyType.requestTypes(cb));}

    /**
     * Shadows method {@link VolleyStats#requestMinDate(StatCallback)}.
     * @param cb is of type {@link StatCallback} and is the object that receives a callback from Volley.
     */
    public void requestMinDate(StatCallback cb) {addToRequestQueue(VolleyStats.requestMinDate(cb));}

    /**
     * Shadows method {@link VolleyStats#requestMaxDate(StatCallback)}.
     * @param cb is of type {@link StatCallback} and is the object that receives a callback from Volley.
     */
    public void requestMaxDate(StatCallback cb) {addToRequestQueue(VolleyStats.requestMaxDate(cb));}

    /**
     * Shadows method {@link VolleyNote#post(String, long, NoteCallback)}.
     *
     * @param note is of type {@link String} and is the note text.
     * @param typeId is of primitive type long and is the remote ID of the note type.
     * @param cb is of type {@link NoteCallback} and is the object that receives a callback from Volley.
     */
    public void postNote(String note, long typeId, NoteCallback cb) {
        addToRequestQueue(VolleyNote.post(note, typeId, cb));
    }

    /**
     * Shadows method {@link VolleyNote#deleteNotes(long)}.
     * @param id is of primtive type long and is the remote ID of the note to delete.
     */
    public void deleteNote(long id) {addToRequestQueue(VolleyNote.deleteNotes(id));}

    /**
     * Shadows method {@link VolleyNote#requestByPage(boolean, int, NoteCallback, Resources)}.
     *
     * @param page is of primitive type int and is the page to display.
     * @param cb is of type {@link NoteCallback} and is the object that receives a callback from Volley.
     * @param res
     */
    public void requestNotes(int page, NoteCallback cb, Resources res) {
        addToRequestQueue(VolleyNote.requestByPage(page == 0, page, cb, res));
    }

    /**
     * Shadows method {@link VolleyNote#requestByDateRange(boolean, int, long, long, NoteCallback, Resources)}.
     *
     * @param page is of primitive type int and is the page to display.
     * @param range is of type {@link DateRange} and is the dates in which notes were written.
     * @param cb is of type {@link NoteCallback} and is the object that receives a callback from Volley.
     * @param res is of type {@link Resources}.
     */
    public void requestNotesByDates(int page, DateRange range, NoteCallback cb, Resources res) {
        final long from = range.getStart();
        final long to = range.getEnd();

        Request r = VolleyNote.requestByDateRange(page == 0, page, from, to, cb, res);
        addToRequestQueue(r);
    }

    /**
     * Shadows method {@link VolleyNote#requestByKeywords(boolean, int, List, NoteCallback, Resources)}.
     *
     * @param page is of primitive type int and is the page to display.
     * @param keywords is of type {@link List<String>} and is a list of keywords to search for.
     * @param cb is of type {@link NoteCallback} and is the object that receives a callback from Volley.
     * @param res is of type {@link Resources}.
     */
    public void requestNotesByKeywords(int page,
                                       List<String> keywords, NoteCallback cb, Resources res) {
        addToRequestQueue(VolleyNote.requestByKeywords(page == 0, page, keywords, cb, res));
    }

    /**
     * Adds Volley requestTypes to queue {@link #mRequestQueue}
     * @param req is of type {@link Request <T>} and is the requestTypes to add to the queue.
     */
    private <T> void addToRequestQueue(Request<T> req) {
        req.setTag(getClass().getSimpleName());
        getRequestQueue().add(req);
    }

    //Getter
    private RequestQueue getRequestQueue() {return mRequestQueue;}
}
