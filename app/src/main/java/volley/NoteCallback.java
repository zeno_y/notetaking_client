package volley;

import java.util.List;

import model.Note;
import model.Type;

/**
 * Created by Zeno Yuki on 6/12/18.
 */
public interface NoteCallback {
    /**
     * Fires when new list (page zero) of notes are received.
     * @param notesList is of type {@link List < Note >} and is a list of notes on the page zero
     */
    void onNotesReceived(List<Note> notesList);

    /**
     * Fires when additional pages for current noteset is received.
     * @param additionalNotes is of type {@link List<Note>} and is a list of additional notes (page > 0)
     */
    void onUpdateReceived(List<Note> additionalNotes);

    /**
     * Fires when response is received from posted note.
     * @param note is of type {@link Note} and is the note to post to remote.
     */
    void onNotePosted(Note note);

    /**
     * Fires when response is received from deleting note(s).
     * @param count is of primitive type int and is the number of notes deleted.
     */
    void onNotesDeleted(int count);

    /**
     * Fires when an error response is received from request.
     * @param errorMsg is of type {@link String} and is the error message.
     * @param clearList is of primitive type boolean and is true if error response requires
     *                  the list to clear.
     */
    void onNotesFailure(String errorMsg, boolean clearList);
}
