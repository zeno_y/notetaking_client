package volley;

import java.util.List;

import model.Note;
import model.Type;

/**
 * Created by Zeno Yuki on 6/12/18.
 */
public interface TypeCallback {
    /**
     * Fires when types list is received and one type is read.
     * @param type is of type {@link Type} of a note.
     */
    void onTypeReceived(Type type);

    /**
     * Fires when new type is posted to remote.
     * @param type is of type {@link Type} and is the new type to store in remote.
     */
    void onTypePosted(Type type);

    void onTypeFailure(String errorMsg);
}
