package volley;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Stores constants used throughout Volley objects.
 * Implement this interface to obtain access to shared constants.
 */
interface VolleyConst {
    //Root of remote API
    String ROOT = "https://apps.zenoyuki.com/NotrRest/";
}
