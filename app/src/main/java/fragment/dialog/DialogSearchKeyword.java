package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

import util.ParsedKeys;

/**
 * Created by Zeno Yuki on 4/23/18.
 *
 * Displays Dialog used when searching for keywords. Extends {@link DialogWrapper} for easier
 * implementation and structure unification.
 *
 * How this Dialog works:
 * Input keywords (separated by spaces) into the Edit text. Once the positive button is pressed
 * (defined in Super) the dialog will parse all keywords using {@link ParsedKeys} and send data
 * off to
 *
 * Implements {@link TextWatcher} to prevent positive button from enabling until some text is provided.
 */
public class DialogSearchKeyword extends DialogWrapper implements TextWatcher {
    //Views
    private EditText mEditText;

    /**
     * Fires when new instance of this Dialog is requested. Does nothing special.
     * @return is of this type and is a new instance.
     */
    public static DialogSearchKeyword newInstance() {
        Bundle args = new Bundle();
        DialogSearchKeyword fragment = new DialogSearchKeyword();

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires when view completes inflation.
     * First calls Super to handle shared Views (like pos/neg buttons) then deals with Views
     * specific to this Dialog.
     *
     * @param view is of type {@link View} and is the view returned
     *             from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enablePosBtn(false);

        mEditText = (EditText)view.findViewById(R.id.dialog_keyword_edittext);
        mEditText.addTextChangedListener(DialogSearchKeyword.this);
    }

    /**
     * Implemented from {@link TextWatcher}. Enables positive button when input text is longer than
     * one character.
     *
     * @param s is of type {@link Editable} and is the text the application reads after user makes
     *          text changes.
     */
    @Override
    public void afterTextChanged(Editable s) {enablePosBtn(s.length() > 0);}

    /**
     * Implemented from Super. Notifies whether or not padding is required for root viewgroup.
     * @return is of primitive type boolean and is true if padding is required.
     */
    @Override
    protected boolean setPaddingForContent() {return true;}

    /**
     * Implemented from Super. Notifies layouts to use (to add to Super's layout root viewgroup)
     * @return is of primitive array type int and is the layouts to inflate in Super.
     */
    @Override
    protected int[] getContentLayouts() {return new int[] {R.layout.dialog_keyword_0_edittext};}

    /**
     * Implemented from Super. Notifies Dialog's Width LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.MATCH_PARENT;}

    /**
     * Implemented from Super. Notifies Dialog's Height LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Implemented from Super. Used in stead of {@link View.OnClickListener#onClick(View)} to prevent
     * confusion.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button.
     */
    @Override
    protected void onBtnClick(int btnId) {
        switch(btnId) {
            case R.id.dialog_pos_btn:
                //Uses progress available in FragmentLoading, instead of the one built into Super.
                final Editable e = mEditText.getText();
                if(getActivity() == null || e == null) return;

                ParsedKeys keys = new ParsedKeys(e.toString());
                ((MainActivity)getActivity()).requestInitNotesByKeywords(keys);
                //falls through
            case R.id.dialog_neg_btn:
                dismiss();
        }
    }

    /**
     * Notifies super of the view that wants to deal with the soft input (on-screen keyboard)
     * @return is nullable and of type {@link View}
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return mEditText;}

    //Unused methods implemented from TextWatcher
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
