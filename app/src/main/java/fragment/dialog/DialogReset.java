package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

/**
 * Created by Zeno Yuki on 5/11/18.
 *
 * Displays Dialog with warning message for when user desired to reset search. Extends
 * {@link DialogWrapper} for easier implementation and structure unification.
 */
public class DialogReset extends DialogWrapper {
    /**
     * Fires when new instance of this Dialog is requested. Does nothing special.
     * @return is of this type and is a new instance.
     */
    public static DialogReset newInstance() {
        Bundle args = new Bundle();
        DialogReset fragment = new DialogReset();

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Implemented from Super. Notifies whether or not padding is required for root viewgroup.
     * @return is of primitive type boolean and is true if padding is required.
     */
    @Override
    protected boolean setPaddingForContent() {return true;}

    /**
     * Implemented from Super. Notifies layouts to use (to add to Super's layout root viewgroup)
     * @return is of primitive array type int and is the layouts to inflate in Super.
     */
    @Override
    protected int[] getContentLayouts() {return new int[] {R.layout.dialog_reset_0_textview};}

    /**
     * Implemented from Super. Notifies Dialog's Width LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.MATCH_PARENT;}

    /**
     * Implemented from Super. Notifies Dialog's Height LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Implemented from Super. Used in stead of {@link View.OnClickListener#onClick(View)} to prevent
     * confusion.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button.
     */
    @Override
    protected void onBtnClick(int btnId) {
        switch(btnId) {
            case R.id.dialog_pos_btn:
                //Uses progress available in FragmentLoading, instead of the one built into Super.
                if(getActivity() == null) return;
                ((MainActivity)getActivity()).requestInitNotes();
                //falls through
            case R.id.dialog_neg_btn:
                dismiss();
        }
    }


    /**
     * Notifies super of the view that wants to deal with the soft input (on-screen keyboard)
     * @return is nullable and of type {@link View}
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return null;}
}
