package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

/**
 * Created by Zeno Yuki on 5/16/18.
 */
public class DialogDeleteAll extends DialogWrapper {
    public static final long DELETE_ALL = -100L;

    /**
     * Fires when new instance of this Dialog is requested. Does nothing special.
     * @return is of this type and is a new instance.
     */
    public static DialogDeleteAll newInstance() {

        Bundle args = new Bundle();

        DialogDeleteAll fragment = new DialogDeleteAll();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected int[] getContentLayouts() {
        return new int[] {R.layout.dialog_delete_0_textview};
    }

    @Override
    protected boolean setPaddingForContent() {return true;}

    @Override
    protected int getWidthLayoutParams() {return ViewGroup.LayoutParams.MATCH_PARENT;}

    @Override
    protected int getHeightLayoutParams() {return ViewGroup.LayoutParams.WRAP_CONTENT;}

    @Override
    protected void onBtnClick(int btnId) {
        switch(btnId) {
            case R.id.dialog_pos_btn:
                //Should use progress in FragmentLoading instead
//                showProgress(true);
//                enablePosBtn(false);

                if(getActivity() == null) return;
                ((MainActivity)getActivity()).deleteNote(DELETE_ALL);
                //falls through
            case R.id.dialog_neg_btn:
                dismiss();
        }
    }

    @Nullable
    @Override
    protected View getViewForSoftInput() {
        return null;
    }
}
