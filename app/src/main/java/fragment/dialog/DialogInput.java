package fragment.dialog;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

import java.util.ArrayList;
import java.util.List;

import adapter.TypeAdapter;
import model.Type;
import util.EscapedChars;

/**
 * Created by Zeno Yuki on 4/18/18.
 * Displays dialog when user desired to store a new Note on the remote. Extends {@link DialogWrapper}
 * for easier implementation and structure unification.
 *
 * Implements {@link TextWatcher} to prevent positive button from enabling until some text is provided.
 */
public class DialogInput extends DialogWrapper implements TextWatcher {
    //Constants. Keys for retrieving arguments
    private static final String KEY_TYPES = "types_key";

    //Views
    private EditText mEditText;
    private Spinner mDropdown;

    /**
     * Fires when new instance of this Dialog is requested.
     *
     * @param typesList is of type {@link List<Type>} and is the list of Note types available.
     * @return is of this type and is a new instance.
     */
    public static DialogInput newInstance(List<Type> typesList) {
        Bundle args = new Bundle();
        DialogInput fragment = new DialogInput();

        args.putParcelableArrayList(KEY_TYPES, (ArrayList<? extends Parcelable>)typesList);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires when view completes inflation.
     * First calls Super to handle shared Views (like pos/neg buttons) then deals with Views
     * specific to this Dialog.
     *
     * @param view is of type {@link View} and is the view returned
     *             from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enablePosBtn(false); //Prevents use from moving forward without text input.

        mEditText = (EditText)view.findViewById(R.id.dialog_input_edittext);
        mEditText.addTextChangedListener(DialogInput.this);

        if(getActivity() == null || getArguments() == null || getArguments().isEmpty()) return;
        List<Type> types = getArguments().getParcelableArrayList(KEY_TYPES);

        if(types == null) return;
        TypeAdapter adapter = new TypeAdapter(requireContext(), R.layout.item_type, types);

        mDropdown = (Spinner)view.findViewById(R.id.dialog_input_dropdown);
        mDropdown.setAdapter(adapter);
    }

    /**
     * Implemented from {@link TextWatcher}. Enables positive button when input text is longer than
     * one character.
     *
     * @param s is of type {@link Editable} and is the text the application reads after user makes
     *          text changes.
     */
    @Override
    public void afterTextChanged(Editable s) {enablePosBtn(s.length() > 0);}

    /**
     * Implemented from Super. Notifies whether or not padding is required for root viewgroup.
     * @return is of primitive type boolean and is true if padding is required.
     */
    @Override
    protected boolean setPaddingForContent() {return true;}

    /**
     * Implemented from Super. Notifies layouts to use (to add to Super's layout root viewgroup)
     * @return is of primitive array type int and is the layouts to inflate in Super.
     */
    @Override
    protected int[] getContentLayouts() {
        return new int[] {R.layout.dialog_input_0_edittext, R.layout.dialog_input_1_dropdown};
    }

    /**
     * Implemented from Super. Notifies Dialog's Width LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.MATCH_PARENT;}

    /**
     * Implemented from Super. Notifies Dialog's Height LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Implemented from Super. Used in stead of {@link View.OnClickListener#onClick(View)} to prevent
     * confusion.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button.
     */
    @Override
    protected void onBtnClick(int btnId) {
        switch(btnId) {
            case R.id.dialog_pos_btn:
                showProgress(true);
                enablePosBtn(false);

                final Editable e = mEditText.getText();
                if(getActivity() == null || e == null) return;

                EscapedChars escaped = new EscapedChars(e.toString());
                ((MainActivity)getActivity()).postNote(
                        escaped.getEscapedText(), mDropdown.getSelectedItemId()
                );
                //Dismissed by MainActivity once response received by remote.
                break;
            case R.id.dialog_neg_btn:
                dismiss();
        }
    }

    /**
     * Notifies super of the view that wants to deal with the soft input (on-screen keyboard)
     * @return is nullable and of type {@link View}
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return mEditText;}

    //Unused methods implemented from TextWatcher
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
