package fragment.dialog;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

import java.util.Calendar;

import util.DateRange;

/**
 * Created by Zeno Yuki on 4/23/18.
 *
 * Displays dialog used when searching for notes within a specific date-range. Extends
 * {@link DialogWrapper} for easier implementation and structure unification.
 *
 * How this
 *
 * Implements {@link CalendarView.OnDateChangeListener} to retrieve the selected date.
 */
public class DialogSearchDate extends DialogWrapper implements CalendarView.OnDateChangeListener {
    //Constants. Keys for retrieving arguments
    private static final String KEY_MIN_DATE = "min_date";
    private static final String KEY_MAX_DATE = "max_date";

    //Views
    private TextView mHeader;
    private ViewSwitcher mSwitcher;
    private CalendarView mStartCal, mEndCal;
    private AppCompatImageButton mToStartDateBtn, mToEndDateBtn;

    //Variables used to extract date from CalendarView through a Listener
    private long mSelectedStartDate = 0L;
    private long mSelectedEndDate = 0L;

    /**
     * Fires when new instance of this Dialog is requested. Does nothing special.
     *
     * @param minDate is of primitive type long and is the least recent date in which a Note was
     *                create in the remote.
     * @param maxDate is of primitive type long and is the most recent date in which a Note was
     *                created in the remote.
     * @return is of this type and is a new instance along with bundled arguments (data retrieved later)
     */
    public static DialogSearchDate newInstance(long minDate, long maxDate) {
        Bundle args = new Bundle();
        DialogSearchDate fragment = new DialogSearchDate();

        args.putLong(KEY_MIN_DATE, minDate);
        args.putLong(KEY_MAX_DATE, maxDate);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fires when view completes inflation.
     * First calls Super to handle shared Views (like pos/neg buttons) then deals with Views
     * specific to this Dialog.
     *
     * @param view is of type {@link View} and is the view returned
     *             from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState is of type {@link Bundle} and contains this dialog's saved state
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mHeader = view.findViewById(R.id.dialog_dates_header);

        mSwitcher = view.findViewById(R.id.dialog_dates_switcher);

        mToStartDateBtn = view.findViewById(R.id.dialog_dates_to_start_btn);
        mToEndDateBtn = view.findViewById(R.id.dialog_dates_to_end_btn);

        mToStartDateBtn.setOnClickListener(DialogSearchDate.this);
        mToEndDateBtn.setOnClickListener(DialogSearchDate.this);

        mToStartDateBtn.setEnabled(false);
        mToEndDateBtn.setEnabled(true);

        mStartCal = view.findViewById(R.id.dialog_dates_start_selector);
        mEndCal = view.findViewById(R.id.dialog_dates_end_selector);

        //Limits both calendars on selectable dates depending on params pass in newInstance(long, long)
        if(getArguments() != null) {
            final long minDate = getArguments().getLong(KEY_MIN_DATE);
            final long maxDate = getArguments().getLong(KEY_MAX_DATE);

            mStartCal.setMinDate(minDate);
            mStartCal.setMaxDate(maxDate);

            mEndCal.setMinDate(minDate);
            mEndCal.setMaxDate(maxDate);
        }

        mStartCal.setOnDateChangeListener(DialogSearchDate.this);
        mEndCal.setOnDateChangeListener(DialogSearchDate.this);
    }

    /**
     * Implemented from Super. Notifies layouts to use (to add to Super's layout root viewgroup)
     * @return is of primitive array type int and is the layouts to inflate in Super.
     */
    @Override
    protected int[] getContentLayouts() {
        return new int[] {
                R.layout.dialog_dates_0_date_type,
                R.layout.dialog_dates_1_calendarview,
                R.layout.dialog_dates_2_btns
        };
    }

    /**
     * Implemented from Super. Notifies whether or not padding is required for root viewgroup.
     * @return is of primitive type boolean and is true if padding is required.
     */
    @Override
    protected boolean setPaddingForContent() {return false;}

    /**
     * Implemented from Super. Notifies Dialog's Width LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getWidthLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Implemented from Super. Notifies Dialog's Height LayoutParam.
     * @return is of primitive type int and is a LayoutParam value.
     */
    @Override
    protected int getHeightLayoutParams() {return LinearLayout.LayoutParams.WRAP_CONTENT;}

    /**
     * Implemented from Super. Used in stead of {@link View.OnClickListener#onClick(View)} to prevent
     * confusion.
     *
     * @param btnId is of primitive type int and is the ID of the clicked button.
     */
    @Override
    protected void onBtnClick(int btnId) {
        switch(btnId) {
            case R.id.dialog_dates_to_start_btn:
                setViewToInitState(true);
                break;
            case R.id.dialog_dates_to_end_btn:
                setViewToInitState(false);
                break;
            case R.id.dialog_pos_btn:
                //Uses progress available in FragmentLoading, instead of the one built into Super.
                final long from = getSelectedStartDate() > 0L ? getSelectedStartDate() : mStartCal.getDate();
                final long to = getSelectedEndDate() > 0L ? getSelectedEndDate() : mEndCal.getDate();
                DateRange range = new DateRange(from, to);

                if(getActivity() == null) return;
                ((MainActivity)getActivity()).requestInitNotesByDateRange(range);
                //falls through
            case R.id.dialog_neg_btn:
                dismiss();
        }
    }

    /**
     * Notifies super of the view that wants to deal with the soft input (on-screen keyboard)
     * @return is nullable and of type {@link View}
     */
    @Nullable
    @Override
    protected View getViewForSoftInput() {return null;}

    /**
     * Implemented from {@link CalendarView.OnDateChangeListener}. Fires when the selected date changes.
     * Used to determine which date is currently selected by the person using the application.
     *
     * @param view is non-null and is the CalendarView who's date was just changed by the user.
     * @param year is of primitive type int and is the year of the selected date.
     * @param month is of primitive type int and is the month of the selected date.
     * @param dayOfMonth is of primitive type int and is the day of the selected date.
     */
    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);

        switch(view.getId()) {
            //Sets date for the range start calendar.
            case R.id.dialog_dates_start_selector:
                setSelectedStartDate(c.getTimeInMillis());
                break;
            //Sets date for th range end calendar.
            case R.id.dialog_dates_end_selector:
                setSelectedEndDate(c.getTimeInMillis());
                break;
        }
    }

    //Getters & Setters
    public long getSelectedStartDate() {return mSelectedStartDate;}
    public void setSelectedStartDate(long startDate) {mSelectedStartDate = startDate;}
    public long getSelectedEndDate() {return mSelectedEndDate;}
    public void setSelectedEndDate(long endDate) {mSelectedEndDate = endDate;}

    /**
     * Switches views from one state to another. Used when switching from start/end calendar.
     * Affects the {@link #mSwitcher}, a {@link ViewSwitcher}, the switching of {@link AppCompatImageButton}
     * {@link #mToStartDateBtn} and {@link #mToEndDateBtn}, and the header {@link #mHeader}, a {@link TextView}
     *
     * @param initState is of primitive type boolean and is true if the initial state of all these
     *                  previously mentioned views is desired.
     */
    private void setViewToInitState(boolean initState) {
        mHeader.setText(initState ? R.string.dialog_dates_pick_start : R.string.dialog_dates_pick_end);

        if(initState) mSwitcher.showPrevious();
        else mSwitcher.showNext();

        mToStartDateBtn.setEnabled(!initState);
        mToEndDateBtn.setEnabled(initState);

        @DrawableRes int startDrawableRes = initState ? R.drawable.ic_vector_left_disabled : R.drawable.ic_vector_left_enabled;
        mToStartDateBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(), startDrawableRes));

        @DrawableRes int endDrawableRes = initState ? R.drawable.ic_vector_right_enabled : R.drawable.ic_vector_right_disabled;
        mToEndDateBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(), endDrawableRes));
    }
}
