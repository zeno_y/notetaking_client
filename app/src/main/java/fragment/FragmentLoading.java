package fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zenoyuki.notr.R;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Displays undefined progress bar until another fragment replaces this one. Does nothing but
 * inflate the layout.
 */
public class FragmentLoading extends Fragment {

    /**
     * Fires when new instance of this Fragment is requested. Does nothing special.
     * @return is of this type and is a new instance.
     */
    public static FragmentLoading newInstance() {
        Bundle args = new Bundle();
        FragmentLoading fragment = new FragmentLoading();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Inflates Fragment view
     *
     * @param inflater is non-null of type {@link LayoutInflater} and is used to inflate this
     *                 Fragment's layout.
     * @param parent is nullable of type {@link ViewGroup} and is this Fragment's parent view.
     * @param savedInstanceState is nullable of type {@link Bundle} and is the previously stored
     *                           instance state.
     * @return is of type {@link View}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loading, parent, false);
    }
}
