package fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zenoyuki.notr.MainActivity;
import com.zenoyuki.notr.R;

import java.util.ArrayList;
import java.util.List;

import adapter.NoteAdapter;
import custom.RecyclerViewEmptySupport;
import model.Note;

/**
 * Created by Zeno Yuki on 4/18/18.
 *
 * Handles displaying notes using a {@link RecyclerViewEmptySupport}. Uses {@link SwipeRefreshLayout}
 * that allows person to swipe down to retrieve new notes by page. Uses new instances to "refresh"
 * the notes list and uses method {@link #insert(List)}.
 */
public class FragmentNotesList extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    //Field for ease-of-reading.
    private static final int STARTING_NEXT_PAGE = 1;
    //Key for storing on Bundles
    private static final String KEY_LIST = "list_key";

    private SwipeRefreshLayout mRefresher;

    private RecyclerViewEmptySupport mRecycler;
    private NoteAdapter mAdapter;

    //new instance is always "page zero". Next page should always start at 1
    private int mNextPage = STARTING_NEXT_PAGE;

    /**
     * Fires when new instance of this Fragment is requested.
     *
     * @param notesList is non-null of type {@link List<Note>} and is a new set of lists to display.
     * @return is of this type and is a new instance.
     */
    public static FragmentNotesList newInstance(@NonNull List<Note> notesList) {
        Bundle args = new Bundle();
        FragmentNotesList fragment = new FragmentNotesList();

        args.putParcelableArrayList(KEY_LIST, (ArrayList<? extends Parcelable>)notesList);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Inflates Fragment view
     *
     * @param inflater is non-null of type {@link LayoutInflater} and is used to inflate this
     *                 Fragment's layout.
     * @param parent is nullable of type {@link ViewGroup} and is this Fragment's parent view.
     * @param savedInstanceState is nullable of type {@link Bundle} and is the previously stored
     *                           instance state.
     * @return is of type {@link View}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, parent, false);
    }

    /**
     * Fires when view is destroyed. Unregisters Oberserver in {@link RecyclerViewEmptySupport} before
     * calling Super.
     */
    @Override
    public void onDestroyView() {
        if(mRecycler != null) mRecycler.unregisterObserver();
        super.onDestroyView();
    }

    /**
     * Sets up {@link SwipeRefreshLayout}, {@link NoteAdapter} and {@link RecyclerViewEmptySupport}
     *
     * @param view is non-null of type {@link View} and is the inflated layout
     *             in {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState is nullable of type {@link Bundle} and is the previously stored
     *                           instance state.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //Sets up refresher layout
        mRefresher = (SwipeRefreshLayout)view.findViewById(R.id.main_refresher);
        mRefresher.setOnRefreshListener(FragmentNotesList.this);
        mRefresher.setColorSchemeColors(
                ContextCompat.getColor(requireContext(), R.color.color_primary),
                ContextCompat.getColor(requireContext(), R.color.color_secondary),
                ContextCompat.getColor(requireContext(), R.color.color_tertiary)
        );

        //Retrieves list from Arguments
        List<Note> list = new ArrayList<>();
        if(getArguments() != null) list = getArguments().getParcelableArrayList(KEY_LIST);

        //Sets up Adapter
        mAdapter = new NoteAdapter(list, R.layout.item_note);

        //Sets up recycler
        mRecycler = (RecyclerViewEmptySupport)view.findViewById(R.id.main_recycler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setEmptyView(view.findViewById(R.id.empty_view));

        mRecycler.setHasFixedSize(false);
        mRecycler.setLayoutManager(new LinearLayoutManager(
                requireContext(), LinearLayoutManager.VERTICAL, true
        ));
    }

    /**
     * Adds one {@link Note} to the currently existing/displayed list.
     * Checks if {@link #mAdapter} is null.
     *
     * @param note is of type {@link Note} and is the note to add.
     */
    public void insert(Note note) {if(mAdapter != null) mAdapter.addItemAsFirst(note);}

    /**
     * Adds a list of {@link Note} to the currently existing/displayed list.
     * Checks if {@link #mAdapter} is null.
     *
     * @param notes is of type {@link List<Note>} and is the list of notes to add.
     */
    public void insert(@NonNull List<Note> notes) {
        if(!notes.isEmpty()) {
            mNextPage++;
            if(mAdapter != null) mAdapter.addNextList(notes);
        }
    }

    /**
     * Stops refresh animation in {@link SwipeRefreshLayout}
     */
    public void stopRefreshing() {if(mRefresher != null) mRefresher.setRefreshing(false);}

    /**
     * Fires when person swipes {@link RecyclerViewEmptySupport} down at list scroll max.
     * Implemented from {@link SwipeRefreshLayout.OnRefreshListener}
     */
    @Override
    public void onRefresh() {
        Log.d("refresh", "requesting page " + mNextPage);
        if(getActivity() != null) ((MainActivity)getActivity()).requestNextPage(mNextPage);
    }
}