package adapter;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zenoyuki.notr.R;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

import custom.NoteView;
import model.Note;

/**
 * Created by Zeno Yuki on 4/18/18.
 *
 * Extends {@link RecyclerView.Adapter} and processes each {@link Note} data onto {@link NoteView}.
 * Adds spacers of count {@link #SPACER_COUNT} to the bottom/beginning (always, never elsewhere) of
 * the attached recyclerview. Adapter count compensates for {@link #SPACER_COUNT}.
 *
 * For every {@link NoteView} the adapter sets the background color and content texts via
 * {@link NoteHolder}. Spacers don't need any processing, and are held by {@link SpacerHolder}
 *
 * Unless specified, methods with annotation @Override is overriding this class' Super.
 */
public class NoteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SPACER_COUNT = 1; //Number of spacers to display.

    private static final int TYPE_SPACER = 0;
    private static final int TYPE_CONTENT = 1;

    private List<Note> mList;
    private @LayoutRes int mRes;

    /**
     * Constructor; one and only. Stores notes list and layout resource to use on each Note.
     *
     * @param list is of type {@link List<Note>} and is the list of notes to display.
     * @param layoutRes is of primitive type int and is the resource ID of the layout to use with
     *                  each list item.
     */
    public NoteAdapter(List<Note> list, @LayoutRes int layoutRes) {
        mList = list;
        mRes = layoutRes;
    }

    /**
     * Inflates new view and attaches to new holder.
     *
     * @param parent is of type {@link ViewGroup} and is the parent of this adapter (the recyclerview)
     * @param viewType is of primitive type int and is the type returned by {@link #getItemViewType(int)}
     * @return is non-null and subclass of type {@link RecyclerView.ViewHolder}.
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch(viewType) {
            //Creates Spacer holder
            case TYPE_SPACER:
                final View space = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_spacer, parent, false
                );

                return new SpacerHolder(space);
            //Default case creates Note holder
            default:
                final View item = LayoutInflater.from(parent.getContext()).inflate(mRes, parent, false);
                return new NoteHolder(item);
        }
    }

    /**
     * Retrieves item view type depending on adapter position (do not mistake with list position).
     * Only returns {@link #TYPE_SPACER} when position is less than
     *
     * @param position is of primitive type int and is the view type of adapter item at
     *                 position {@param position}
     * @return is of primitive type int
     */
    @Override
    public int getItemViewType(int position) {
        if(position < SPACER_COUNT) return TYPE_SPACER;
        else return TYPE_CONTENT;
    }

    /**
     * Binds view holder with content.
     *
     * @param holder is non-null and a subclass of {@link RecyclerView.ViewHolder}
     * @param position is of primitive type int and is the adapter item at adapter position that is
     *                 currently being processed.
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //Ignores everything if position is less than the count of spacers
        //Spacers are always at the beginning of the adapter list.
        if(position < SPACER_COUNT) return;

        //Retrieves item from item list, accounting for spacers
        Note note = getItem(position - SPACER_COUNT);
        if(note == null) return;

        //Formats dates and times based on long date values in Note
        final CharSequence date = DateFormat
                .getDateInstance(DateFormat.MEDIUM, Locale.JAPAN).format(note.getDate());
        final CharSequence time = DateFormat
                .getTimeInstance(DateFormat.SHORT, Locale.JAPAN).format(note.getDate());

        //Tries to handle ClassCast Exception for calling NoteHolder methods
        try {
            final String timestamp = ((NoteHolder)holder).getContext()
                    .getResources().getString(R.string.server_time, date, time);

            ((NoteHolder)holder).setText(timestamp, note.getNote());
            ((NoteHolder)holder).setColor(findColorResByType(note.getTypeId()));
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves item adapter item count, taking into consideration the number of spacers to display.
     * @return is of primitive type int and is the number of items the adapter must handle.
     */
    @Override
    public int getItemCount() {
        return mList != null && !mList.isEmpty() ? mList.size() + SPACER_COUNT : 0;
    }

    /**
     * Adds Note item to list. Notifies adapter of change in dataset.
     * @param note is of type {@link Note} and is the new note to add to the list.
     */
    public void addItemAsFirst(Note note) {
        mList.add(0, note);
        notifyDataSetChanged();
    }

    /**
     * Adds a list of notes to the existing list. Notifies adapter of change in dataset.
     * @param notes is of type {@link List<Note>} and is a list of new notes to add to the list
     *              (most likely a list of notes that came from pulling a new page of note content).
     */
    public void addNextList(List<Note> notes) {
        mList.addAll(notes);
        notifyDataSetChanged();
    }

    /**
     * Retrieves {@link Note} item from list.
     *
     * @param position is of primitive type int and is the position of the {@link Note} in the list
     *                 to retrieve.
     * @return is nullable and is of type {@link Note} at list position {@param position}
     */
    @Nullable
    private Note getItem(int position) {return mList != null ? mList.get(position) : null;}

    /**
     * Retrieves color of {@link NoteView} background. Dependent on {@link Note#getTypeId()}
     *
     * @param noteType is of primitive type int and is the type ID of the note.
     * @return is of primitive type int and is the color resource of the note background color.
     */
    @ColorRes
    private int findColorResByType(int noteType) {
        switch(noteType) {
            case Note.TYPE_TODOS:
                return R.color.color_note_type_2;
            //Note.TYPE_THOUGHTS
            default:
                return R.color.color_note_type_1;
        }
    }

    /**
     * Holds spacer views for better performance. Does nothing else.
     * Static since only one Holder should exist per type and adapter.
     */
    static final class SpacerHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor. Calls super.
         * @param itemView is of type {@link View} and is the view (spacer view) to hold.
         */
        SpacerHolder(View itemView) {super(itemView);}
    }

    /**
     * Holds Note views for better performance.
     * Static since only one Holder should exist per type and adapter.
     */
    static final class NoteHolder extends RecyclerView.ViewHolder {
        /**
         * Constructor. Calls super.
         * @param itemView is of type {@link View} and is the view (spacer view) to hold.
         */
        NoteHolder(View itemView) {super(itemView);}

        /**
         * Shadows method {@link NoteView#setText(CharSequence, CharSequence)}. Tries to call said
         * method while checking for ClassCastException.
         *
         * @param date is of type {@link CharSequence} and is a formatted date value to set to the view.
         * @param note is of type {@link CharSequence} and is the note value to set to the view.
         */
        void setText(CharSequence date, CharSequence note) {
            try {
                ((NoteView)itemView).setText(date, note);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }

        /**
         * Shadows method {@link NoteView#setColor(int)}. Tries to call said method while checking
         * for ClassCastException.
         *
         * @param color is if primitive type int and is the color resource ID to set the background to.
         */
        void setColor(@ColorRes int color) {
            try {
                ((NoteView)itemView).setColor(color);
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }

        /**
         * Retrieves context from stored view.
         * @return is of type {@link Context} and is the context of the held view.
         */
        Context getContext() {
            return itemView.getContext();
        }
    }
}
