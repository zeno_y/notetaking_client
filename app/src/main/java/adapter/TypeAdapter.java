package adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zenoyuki.notr.R;

import java.util.List;

import model.Type;

/**
 * Created by Zeno Yuki on 4/21/18.
 *
 * Extends {@link ArrayAdapter} and processes each {@link Type} onto a {@link TextView}.
 * TextView icon/icon color will change depending on the Type ID.
 *
 * Unless specified, methods with annotation @Override is overriding this class' Super.
 */
public class TypeAdapter extends ArrayAdapter<Type> {
    private List<Type> mList;
    @LayoutRes private int mRes;

    /**
     * Constructor; one and only. Calls super and stores list and resource ID for later use.
     *
     * @param context is non-null and of type {@link Context}. Used to call Super.
     * @param resource is of primitive type int and is the resource ID to use for each list item.
     * @param typesList is of type {@link List<Type>} and is the list of items to process.
     */
    public TypeAdapter(@NonNull Context context, int resource, @NonNull List<Type> typesList) {
        super(context, resource, typesList);

        mRes = resource;
        mList = typesList;
    }

    /**
     * Retrieves item list count
     * @return is of primitive type int and is the number if items in the list that the adapter
     *         must process.
     */
    @Override
    public int getCount() {return mList.size();}

    /**
     * Retrieves {@link Type} item from list at position {@param position}
     * @param position is of primitive type int and is the position of the {@link Type} to retrieve
     *                 from the list.
     * @return is nullable and is the {@link Type} item at list position {@param position}
     */
    @Nullable
    @Override
    public Type getItem(int position) {return mList.get(position);}

    /**
     * Retrieves item ID from list at list position {@param position}
     * @param position is of primitive type int and is the position of the {@link Type} in the list
     *                 to retrieve the ID from.
     * @return is of primitive type long and is the ID of the {@link Type} item.
     */
    @Override
    public long getItemId(int position) {return mList.get(position).getId();}

    /**
     * Retrieves view, containing all the necessary data, to display in the ListView. Uses a ViewHolder
     * {@link TextHolder} to hold views, for performance.
     *
     * @param position is of primitive type int and is the adapter position currently being processed.
     * @param row is nullable and of type {@link View} and is the row/view that is currently being processed.
     * @param parent is non-null and of type {@link ViewGroup} and is the parent view of this adapter
     * @return is non-null and is the View to dispaly in the ListView
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View row, @NonNull ViewGroup parent) {
        //Initialize TextHolder object.
        TextHolder holder;

        //Determines whether a view needs to be inflated and associated with the view holder
        // or if holder can be retrieved from the view tag.
        if(row == null || row.getTag() == null) {
            row = LayoutInflater.from(parent.getContext()).inflate(mRes, parent, false);
            holder = new TextHolder(row);

            row.setTag(holder);
        } else holder = (TextHolder)row.getTag();

        Type t = getItem(position);

        if(t != null) {
            holder.setText(t.getType());
            holder.setDrawableStart(getDrawableStart((int)t.getId()));
        }

        return row;
    }

    /**
     * Retrieves drawable to use in TextView, to indicate to the user the color of the note depending
     * on which type he/she selects.
     *
     * @param typeId is of primitive type int nad is the ID of the type
     * @return is of primitive type int and is the drawable resource ID to use in the TextView.
     */
    @DrawableRes
    private int getDrawableStart(int typeId) {
        switch(typeId) {
            case Type.TYPE_TODOS:
                return R.drawable.ic_vector_type_indicator_2;
            default:
                return R.drawable.ic_vector_type_indicator_1;
        }
    }

    /**
     * Holds TextView for better performance of ListViews. Shadows some methods for simpler
     * interface between adapter and view.
     * Static since only one holder should exist per adapter.
     */
    static final class TextHolder {
        TextView mTextView; //Textview that is held by this holder.

        /**
         * Constructor. Stores view for later use.
         * @param v is of type {@link View} and is the view to hold.
         */
        TextHolder(View v) {
            try {
                mTextView = (TextView)v;
            } catch(ClassCastException e) {
                e.printStackTrace();
            }
        }

        /**
         * Shadows method {@link TextView#setText(CharSequence)}.
         * @param text is of type {@link CharSequence} and is the text to set to the TextView
         */
        void setText(CharSequence text) {
            mTextView.setText(text);
        }

        /**
         * Retrieves and sets drawable to use for this TextView.
         * @param res is of primitive type int and is the drawable resource ID to use for the
         *            TextView icon
         */
        void setDrawableStart(@DrawableRes int res) {
            //Retrieves drawable
            Drawable d = VectorDrawableCompat.create(
                    mTextView.getContext().getResources(), res, null
            );

            //Sets drawable bounds and to TextView
            if(d != null) {
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                mTextView.setCompoundDrawables(d, null, null, null);
            }
        }
    }
}
