package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import model.Type;

/**
 * Created by Zeno Yuki on 6/12/18.
 *
 * Provides the rest of the application access to the database; an interface to the database.
 */
public class DataAccess {
    public static final int SIDE_MIN = 0;
    public static final int SIDE_MAX = 1;

    //Constants used as arguments when returning values from the database
    private static final Character SPACE = ' '; //Explicitly define a space character for ease of readability
    private static final String ASCENDING = SPACE + "ASC"; //Used to sort entries in Cursors
    private static final String DESCENDING = SPACE + "DESC"; //Used to sort entries in Cursors

    //Stores an instance of the database
    private SQLiteDatabase mDb;
    //Stores an instance of the database helper
    private DatabaseHelper mHelper;

    /**
     * Constructor. Stores an instance of DatabaseHelper.
     * @param c is of type {@link Context} and is the context of the calling class.
     */
    public DataAccess(Context c) {mHelper =  DatabaseHelper.getInstance(c);}

    /**
     * Opens the database as writable.
     */
    public void open() {mDb = mHelper.getWritableDatabase();}

    /**
     * Closes the database.
     */
    public void close() {if(mDb.isOpen()) mDb.close();}

    /**
     * Stores note type from remote into local database. This method should run on request response,
     * not request, so that ID of item in remote is also stored here.
     *
     * @param type is of type {@link Type} and is a note type retrieved from remote
     * @return is of primitive type long and is the ID of the new/replaced item. -1 if error occurred.
     */
    public long putType(Type type) {
        ContentValues values = new ContentValues();

        values.put(TableTypes.KEY_ID, type.getId());
        values.put(TableTypes.TYPE, type.getType());

        return getDb().replace(TableTypes.TABLE_NAME, null, values);
    }

    /**
     * Stores date for a respective side of the date-range.
     * 
     * @param side is of primitive type int and is either {@link #SIDE_MIN} or {@link #SIDE_MAX}.
     * @param date is of primitive type long and is the date for min/max of range.
     * @return is of primitive type long and is ID of the new/replaced item.
     */
    public long putDateOfRange(int side, long date) {
        ContentValues values = new ContentValues();
        Cursor dCursor = getDateCursor(side);

        if(dCursor.moveToFirst()) {
            values.put(TableMinMaxDates.KEY_ID, getLong(dCursor, TableMinMaxDates.KEY_ID));
        }

        values.put(TableMinMaxDates.SIDE, side);
        values.put(TableMinMaxDates.DATE, date);

        return getDb().replace(TableMinMaxDates.TABLE_NAME, null, values);
    }

    /**
     * Retrieves all note types stored in the local database.
     * @return is of type {@link List<Type>} and is a list of all the note types.
     */
    public List<Type> getAllTypes() {
        Cursor tCursor = getTypeCursor();
        List<Type> tList = new ArrayList<>();

        if(tCursor.moveToFirst()) {
            do {
                tList.add(new Type(
                        getLong(tCursor, TableTypes.KEY_ID), getString(tCursor, TableTypes.TYPE))
                );
            } while(tCursor.moveToNext());
        }

        return tList;
    }

    /**
     * Retrieves the min date in the date range.
     * @return is of primitive type long and is the date stored in the database for {@link #SIDE_MIN}.
     */
    public long getMinDateOfRange() {
        Cursor dCursor = getDateCursor(SIDE_MIN);
        return dCursor.moveToFirst() ? getLong(dCursor, TableMinMaxDates.DATE) : -1;
    }

    /**
     * Retrieves the max date in the date range.
     * @return is of primitive type long and is the date stored in the database for {@link #SIDE_MAX}.
     */
    public long getMaxDateOfRange() {
        Cursor dCursor = getDateCursor(SIDE_MAX);
        return dCursor.moveToFirst() ? getLong(dCursor, TableMinMaxDates.DATE) : -1;
    }

    /**
     * Retrieves type cursor to all note types in the database.
     * @return is of type {@link Cursor} and is the cursor pointing to items in the database.
     */
    private Cursor getTypeCursor() {
        return getDb().query(TableTypes.TABLE_NAME,
                TableTypes.ALL_COLUMNS,
                null, null, null, null,
                TableTypes.KEY_ID + ASCENDING);
    }

    /**
     * Retrieves date cursor pointing to the date for the respective side.
     * @param side is of primitive type int and is either {@link #SIDE_MIN} or {@link #SIDE_MAX}
     * @return is of type {@link Cursor} and is the cursor pointing to items in the database.
     */
    private Cursor getDateCursor(int side) {
        return getDb().query(TableMinMaxDates.TABLE_NAME,
                TableMinMaxDates.ALL_COLUMNS,
                TableMinMaxDates.SIDE + " =?", new String[] {String.valueOf(side)},
                null, null,
                TableMinMaxDates.KEY_ID + ASCENDING);
    }

    /**
     * Shadow method for {@link Cursor#getString(int)}
     * Used for making code easier to read.
     *
     * @param c is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides in.
     * @return The value of that column as a String value
     * @see Cursor#getString(int)
     */
    private static String getString(@NonNull Cursor c, @NonNull String columnName) {
        return c.getString(c.getColumnIndex(columnName));
    }

    /**
     * Shadow method for {@link Cursor#getInt(int)}
     * Used for making code easier to read.
     *
     * @param c is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides in.
     * @return The value of that column as an int value
     * @see Cursor#getInt(int)
     */
    private static int getInt(@NonNull Cursor c, @NonNull String columnName) {
        return c.getInt(c.getColumnIndex(columnName));
    }

    /**
     * Shadow method for {@link Cursor#getLong(int)}
     * Used for making code easier to read.
     *
     * @param c is of class {@link Cursor}
     * @param columnName is of class {@link String}. Caution: Method assumes {@param columnName} is
     *                   a valid column within the SQLite database table that {@param c} resides in.
     * @return The value of that column as a long value
     * @see Cursor#getLong(int)
     */
    private static long getLong(@NonNull Cursor c, @NonNull String columnName) {
        return c.getLong(c.getColumnIndex(columnName));
    }

    /**
     * Getter for database instance
     * @return is of type {@link SQLiteDatabase} and is an instance of the database
     */
    private SQLiteDatabase getDb() {return mDb;}
}
