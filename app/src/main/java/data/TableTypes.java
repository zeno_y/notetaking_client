package data;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Zeno Yuki on 6/12/18.
 *
 * Stores the types of notes found in the remote database
 */
class TableTypes {
    //Table columns
    static final String TABLE_NAME = "note_types";
    static final String KEY_ID = "_id";
    static final String TYPE = "type";

    //Array of all table columns
    static final String[] ALL_COLUMNS = new String[] {
            KEY_ID,
            TYPE
    };

    //Table creation string in SQL
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID + " INTEGER PRIMARY KEY, " +
            TYPE + " TEXT UNIQUE NOT NULL);";

    /**
     * Runs when the Database Helper needs this table created.
     * @param db is of type {@link SQLiteDatabase} and is the database to create the table in.
     */
    static void onCreate(SQLiteDatabase db) {db.execSQL(CREATE_TABLE);}

    /**
     * Runs when the Database Helper needs this table to update/upgrade.
     * @param db is of type {@link SQLiteDatabase} and is the database in which the table resides in.
     */
    static void onUpgrade(SQLiteDatabase db) {}
}
