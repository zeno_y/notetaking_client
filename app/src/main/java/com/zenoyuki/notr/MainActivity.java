package com.zenoyuki.notr;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import data.DataAccess;
import fragment.FragmentLoading;
import fragment.FragmentNotesList;
import fragment.dialog.DialogDeleteAll;
import fragment.dialog.DialogInput;
import fragment.dialog.DialogReset;
import fragment.dialog.DialogSearchDate;
import fragment.dialog.DialogSearchKeyword;
import fragment.dialog.DialogWrapper;
import model.Note;
import model.Type;
import util.DateRange;
import util.ParsedKeys;
import volley.NoteCallback;
import volley.Requester;
import volley.StatCallback;
import volley.TypeCallback;

/**
 * Handles relaying information from different classes.
 * Data travels through Activity (bi-directional) from Fragments to Volleys.
 *
 * Handles showing Snackbars to communicate with the person who uses the application.
 *
 * Manages user clicks on FAB button and toolbar menus.
 */
//TODO: search by type
//TODO: Store notes locally; provide syncing features?
//TODO: Add security features to API/Volley
public class MainActivity extends AppCompatActivity implements NoteCallback, TypeCallback, StatCallback {
    //Tag constants for fragment transactions.
    private static final String TAG_FRAGMENT = "fragment";
    private static final String TAG_DIALOG = "dialog";

    //Constants for a switch statement to determine which dialog needs to display.
    private static final int DIALOG_TYPE_INPUT = 0;
    private static final int DIALOG_TYPE_DATE = 1;
    private static final int DIALOG_TYPE_KEYWORD = 2;
    private static final int DIALOG_RESET = 3;
    private static final int DIALOG_DELETE = 4;

    public static final int SEARCH_TYPE_ALL = 5;
    public static final int SEARCH_TYPE_DATES = 6;
    public static final int SEARCH_TYPE_KEYWORDS = 7;

    private int mSearchType = SEARCH_TYPE_ALL;

    //View for anchoring Snackbar.
    private View mSnackbarParent;
    //Reference to main fragment for various uses. Stores reference for quicker/cheaper access
    private FragmentNotesList mMainFragment;

    //Interface for firing when activity loses focus.
    private ActivityInterface mActivityInterface;
    //Variable to track whether the list shown is the result of a search rather than the default list.
    private boolean mSearchShown = false;

    private DateRange mSearchRange;
    private ParsedKeys mSearchKeys;

    //Interrupts fragment transactions if activity has been stopped by framework.
    private boolean mInterrupt;

    /**
     * Fires when Activity is created. Calls super.
     * Finds and sets up required views. Requests the first page of notes.
     *
     * @param savedInstanceState is of type {@link Bundle} and is the previously saved Activity state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSnackbarParent = findViewById(R.id.main_root);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar a = getSupportActionBar();
        if(a != null) {
            a.setTitle(null);
        }

        FloatingActionButton addBtn = (FloatingActionButton)findViewById(R.id.main_btn_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogType(DIALOG_TYPE_INPUT);
            }
        });

        Requester.getInstance(getApplicationContext()).requestTypes(MainActivity.this);
        Requester.getInstance(getApplicationContext()).requestMinDate(MainActivity.this);
        Requester.getInstance(getApplicationContext()).requestMaxDate(MainActivity.this);

        setStatusBarTranslucent(false); //maybe set to true later
    }

    /**
     * Implemented from Super. Sets {@link #mInterrupt} to false before requesting initial set of notes.
     * Fragment Transactions can now occur.
     */
    @Override
    protected void onResume() {
        mInterrupt = false;
        requestInitNotes();

        super.onResume();
    }

    /**
     * Implemented from Super. Sets {@link #mInterrupt} to true. Fragment Transactions are now
     * interrupted.
     */
    @Override
    protected void onStop() {
        mInterrupt = true;
        super.onStop();
    }

    /**
     * Requests the deletion of a note with id of {@param id}. {@link DialogDeleteAll} may pass
     * {@link DialogDeleteAll#DELETE_ALL} to request remote to delete all notes.
     *
     * @param id is of primitive type long and is the ID of the note to delete (or -1 to delete all)
     */
    public void deleteNote(long id) {
        Requester.getInstance(getApplicationContext()).deleteNote(id);
    }

    /**
     * Requests the posting of a note with the data passed through as params.
     *
     * @param note is of type {@link String} and is the note to post.
     * @param typeId is of primitive type long and is the type ID of the note.
     */
    public void postNote(String note, long typeId) {
        Requester.getInstance(getApplicationContext()).postNote(note, typeId, MainActivity.this);
    }

    /**
     * Requests the next page of notes.
     *
     * @param nextPage is of primitive type int and is the new page number to request.
     */
    public void requestNextPage(int nextPage) {
        final int searchType = getSearchType();
        final Requester r = Requester.getInstance(getApplicationContext());

        switch(searchType) {
            case SEARCH_TYPE_ALL:
                r.requestNotes(nextPage, MainActivity.this, getResources());
                break;
            case SEARCH_TYPE_DATES:
                r.requestNotesByDates(nextPage, getSearchRange(),MainActivity.this, getResources());
                break;
            case SEARCH_TYPE_KEYWORDS:
                final List<String> sList = getSearchKeys().getParsedKeys();
                r.requestNotesByKeywords(nextPage, sList,MainActivity.this, getResources());
                break;
        }
    }

    /**
     * Requests all notes, no filter.
     * Transits to loading fragment {@link FragmentLoading} and disables the "reset search" menu option
     * through {@link #setSearchShown(boolean)} & {@link #invalidateOptionsMenu()}
     */
    public void requestInitNotes() {
        setSearchType(SEARCH_TYPE_ALL);

        showLoading();
        Requester.getInstance(getApplicationContext()).requestNotes(
                0, MainActivity.this, getResources()
        );

        //disables "reset search" menu option
        setSearchShown(false);
        invalidateOptionsMenu();
    }

    /**
     * Requests notes by date range.
     * Transits to loading fragment {@link FragmentLoading} and enables the "reset search" menu option
     * through {@link #setSearchShown(boolean)} & {@link #invalidateOptionsMenu()}
     *
     * @param range is of type {@link DateRange} and is the date range to search notes in.
     */
    public void requestInitNotesByDateRange(DateRange range) {
        setSearchType(SEARCH_TYPE_DATES);
        setSearchRange(range);

        showLoading();
        Requester.getInstance(getApplicationContext()).requestNotesByDates(
                0, range, MainActivity.this, getResources()
        );

        //Enables "reset search" menu option
        setSearchShown(true);
        invalidateOptionsMenu();
    }

    /**
     * Requests notes by keywords.
     * Transits to loading fragment {@link FragmentLoading} and enables the "reset search" menu option
     * through {@link #setSearchShown(boolean)} & {@link #invalidateOptionsMenu()}
     *
     * @param keys is of type {@link ParsedKeys} and is the set of keywords to search for.
     */
    public void requestInitNotesByKeywords(ParsedKeys keys) {
        setSearchType(SEARCH_TYPE_KEYWORDS);
        setSearchKeys(keys);

        showLoading();
        Requester.getInstance(getApplicationContext()).requestNotesByKeywords(
                0, keys.getParsedKeys(), MainActivity.this, getResources()
        );

        //Enables "reset search" menu option
        setSearchShown(true);
        invalidateOptionsMenu();
    }

    /**
     * Implemented from {@link NoteCallback}. Fires when first page of request is received.
     * @param notesList is of type {@link List<Note>} and is a list of notes on the page zero
     */
    @Override
    public void onNotesReceived(List<Note> notesList) {showMain(notesList);}

    /**
     * Implemented from {@link NoteCallback}. Fires when other requested pages (pages > 0) are received.
     * @param additionalNotes is of type {@link List<Note>} and is a list of additional notes (page > 0)
     */
    @Override
    public void onUpdateReceived(@NonNull List<Note> additionalNotes) {
        //If FragmentNotesList instance is not found, notify user and do nothing else.
        if(mMainFragment == null) {
            showSnackbar(getString(R.string.msg_no_frag_instance));
            return;
        }

        Log.d("update", "received " + additionalNotes.size() + " notes");
        //Updates FragmentNotesList instance as long as param list is not empty. Notify user otherwise.
        if(additionalNotes.isEmpty()) showSnackbar(getString(R.string.msg_no_more_notes));
        else mMainFragment.insert(additionalNotes);

        //Stops FragmentNotesList SwipeRefreshLayout refresh animation.
        mMainFragment.stopRefreshing();
    }

    /**
     * Implemented from {@link NoteCallback}. Fires when post note request response is received.
     * @param note is of type {@link Note} and is the note to post to remote.
     */
    @Override
    public void onNotePosted(Note note) {
        if(mMainFragment != null) mMainFragment.insert(note);
        dismissDialogs(true);
    }

    /**
     * Implemented from {@link NoteCallback}. Fires when delete note request response is received.
     * @param count is of primitive type int and is the number of notes deleted.
     */
    @Override
    public void onNotesDeleted(int count) {
        showSnackbar(getString(R.string.msg_deleted_notes, count));
        dismissDialogs(true);
    }

    /**
     * Implemented from {@link NoteCallback}. Fires when request response is an error.
     *
     * @param errorMsg is of type {@link String} and is the error message.
     * @param clearList is of primitive type boolean and is true if error response requires
     */
    //TODO: need to make errors more transparent and robust.
    @Override
    public void onNotesFailure(@Nullable String errorMsg, boolean clearList) {
        if(errorMsg == null) errorMsg = "something went wrong with no message";
        showSnackbar(errorMsg);
        dismissDialogs(true);

        if(clearList) showMain(new ArrayList<Note>());
    }

    @Override
    public void onTypeReceived(Type type) {getDataAccess().putType(type);}

    @Override
    public void onTypePosted(Type type) {
        //What happens when app receives response from post?
    }

    @Override
    public void onTypeFailure(String errorMsg) {
        //show snackbar
    }

    @Override
    public void onMinDateReceived(long date) {
        getDataAccess().putDateOfRange(DataAccess.SIDE_MIN, date);
    }

    @Override
    public void onMaxDateReceived(long date) {
        getDataAccess().putDateOfRange(DataAccess.SIDE_MAX, date);
    }

    @Override
    public void onStatFailure(int stat, String errorMsg) {
        //show snackbar
    }

    /**
     * Fires when options menu is about to be created. Inflates menu resource.
     * @param menu is of type {@link Menu} and is the menu to inflate.
     * @return is of primitive type boolean and is true if the menu should be created.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Fires when options menu is being prepared. Fires every time {@link #invalidateOptionsMenu()}
     * is called. Used to enable/disable menu items.
     *
     * @param menu is of type {@link Menu} and is the menu being prepared.
     * @return is of primitive type boolean and is true if  menu should be displayed.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_reset).setEnabled(isSearchShown());
        menu.findItem(R.id.menu_search_date).setEnabled(isDatesValid());
        return true;
    }

    /**
     * Fires when an item in the options menu is selected.
     *
     * @param item is of type {@link MenuItem} and is the item that was selected.
     * @return is of primitive type boolean and is true if the click action is consumed here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.menu_delete_all:
                showDialogType(DIALOG_DELETE);
                return true;
            case R.id.menu_reset:
                showDialogType(DIALOG_RESET);
                return true;
            case R.id.menu_search_date:
                showDialogType(DIALOG_TYPE_DATE);
                return true;
            case R.id.menu_search_keyword:
                showDialogType(DIALOG_TYPE_KEYWORD);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Overrides {@link AppCompatActivity#onWindowFocusChanged(boolean)}
     *
     * Alerts {@link ActivityInterface} when activity window has focus
     * @param hasFocus is of the primitive type boolean and is true if
     *                 this activity window currently has focus.
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(mActivityInterface != null) mActivityInterface.onActivityWindowFocusChanged(hasFocus);
    }

    /**
     * Shows loading fragment, but only after removing any pre-existing fragments.
     */
    private void showLoading() {
        if(mInterrupt) return;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prevFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);

        ft.setCustomAnimations(R.anim.loading_transition_in, R.anim.loading_transition_out);
        if(prevFragment != null) ft.remove(prevFragment);

        ft.add(R.id.main_fragment, FragmentLoading.newInstance(), TAG_FRAGMENT)
                .disallowAddToBackStack()
                .commit();
    }

    /**
     * Shows main fragment, but only after removing any pre-existing fragments.
     * @param notesList is of type {@link List<Note>} and is a list of notes to initially display
     *                  in the fragment.
     */
    private void showMain(List<Note> notesList) {
        if(mInterrupt) return;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prevFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
        mMainFragment = FragmentNotesList.newInstance(notesList);

        ft.setCustomAnimations(R.anim.main_transition_in, R.anim.main_transition_out);
        if(prevFragment != null) ft.remove(prevFragment);

        ft.add(R.id.main_fragment, mMainFragment, TAG_FRAGMENT)
                .disallowAddToBackStack()
                .commit();
    }

    /**
     * Shows a Dialog, depending on parameter value. Uses constants declared at the top of this class.
     * @param type is of primitive type int and is the type of dialog to display.
     */
    private void showDialogType(int type) {
        FragmentTransaction ft = dismissDialogs(false); //Commit occurs at method end.

        DialogWrapper f = null;
        switch(type) {
            case DIALOG_DELETE:
                f = DialogDeleteAll.newInstance();
                break;
            case DIALOG_TYPE_DATE:
                final long min = getDataAccess().getMinDateOfRange();
                final long max = getDataAccess().getMaxDateOfRange();

                if(isDatesValid()) f = DialogSearchDate.newInstance(min, max);
                else showSnackbar(getString(R.string.msg_invalid_dates));
                break;
            case DIALOG_TYPE_KEYWORD:
                f = DialogSearchKeyword.newInstance();
                break;
            case DIALOG_TYPE_INPUT:
                f = DialogInput.newInstance(getDataAccess().getAllTypes());
                break;
            case DIALOG_RESET:
                f = DialogReset.newInstance();
                break;
        }

        if(f != null) {
            setActivityInterface(f);
            f.show(ft, TAG_DIALOG);
        } else ft.commit(); //Should never reach
    }

    /**
     * Supposedly is required to make the Actionbar truly transparent.
     *
     * @param makeTranslucent is of primitive type boolean and is true if app wants to make its
     *                        Actionbar transparent.
     */
    private void setStatusBarTranslucent(boolean makeTranslucent) {
        if(makeTranslucent) getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    /**
     * Dismisses dialogs and commits transaction depending on parameter value.
     *
     * @param commit is of primitive type boolean and is true if calling method requires transaction
     *               to be commited.
     * @return is of type {@link FragmentTransaction} and is the transaction that, so far, has dismissed
     * a dialog that is currently being displayed. May have been committed.
     */
    private FragmentTransaction dismissDialogs(boolean commit) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prevDialog = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG);

        if(prevDialog != null) {
            ft.remove(prevDialog);
            ft.disallowAddToBackStack();
            //Commits if necessary
            if(commit) ft.commit();
        }

        return ft;
    }

    /**
     * Shows Snackbar with a message with background color equal to theme's secondary color.
     * @param message is of type {@link String} and is the message to display.
     */
    private void showSnackbar(String message) {
        Snackbar sb = Snackbar.make(mSnackbarParent, message, Snackbar.LENGTH_SHORT);
        sb.getView().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.color_secondary));
        sb.show();
    }

    /**
     * Determines whether date range received from remote is valid.
     * @return is of primitive type boolean and is true if date range from remote is valid.
     */
    private boolean isDatesValid() {
        return getDataAccess().getMinDateOfRange() > 0L && getDataAccess().getMaxDateOfRange() > 0L;
    }

    //Getters & Setters
    private void setSearchShown(boolean shown) {mSearchShown = shown;}
    private boolean isSearchShown() {return mSearchShown;}
    private void setSearchType(int type) {mSearchType = type;}
    private int getSearchType() {return mSearchType;}
    private void setSearchRange(DateRange range) {mSearchRange = range;}
    private DateRange getSearchRange() {return mSearchRange;}
    private void setSearchKeys(ParsedKeys keys) {mSearchKeys = keys;}
    private ParsedKeys getSearchKeys() {return mSearchKeys;}
    private void setActivityInterface(ActivityInterface ai) {mActivityInterface = ai;}
    private DataAccess getDataAccess() {return ((Universal)getApplicationContext()).getDataAccess();}
}
