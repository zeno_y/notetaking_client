package com.zenoyuki.notr;

import android.app.Application;
import android.database.SQLException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.stetho.Stetho;

import java.util.List;

import data.DataAccess;
import model.Type;
import util.DateRange;
import util.ParsedKeys;
import volley.Requester;
import volley.VolleyNote;
import volley.VolleyStats;
import volley.VolleyType;

/**
 * Created by Zeno Yuki on 4/18/18.
 *
 * Extends Application to allow for database access anywhere that has access to a
 * {@link android.content.Context}.
 *
 * Initializes Stetho. Commented out when not debugging.
 */
public class Universal extends Application {

    private DataAccess mDataAccess;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(Universal.this);
        setDataAccess(new DataAccess(Universal.this));

        try {
            getDataAccess().open();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if(getDataAccess() != null) getDataAccess().close();
    }

    private void setDataAccess(DataAccess da) {mDataAccess = da;}

    public DataAccess getDataAccess() {return mDataAccess;}
}
