package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Models a Type.
 * Type is an attribute of a {@link Note} that categorizes notes.
 */
public class Type implements Parcelable {
    //IDs of each type. Must match data from http://tomcat.zenoyuki.com/NotrRest/all-types
    public static final int TYPE_THOUGHTS = 1;
    public static final int TYPE_TODOS = 2;

    private long mId;
    private String mType;

    /**
     * Private bare constructor. Prevents class being instantiated without params.
     */
    private Type() {}

    /**
     * Constructor.
     *
     * @param mId is of primitive type long and is the remote ID of this type
     * @param mType is of type {@link String} and is the text value of this type.
      */
    public Type(long mId, String mType) {
        this.mId = mId;
        this.mType = mType;
    }

    //Setters and Getters
    public long getId() {return mId;}
    public void setId(long mId) {this.mId = mId;}
    public String getType() {return mType;}
    public void setType(String mType) {this.mType = mType;}

    /**
     * Overrides super {@link Object#toString()} so that, when required, the system will display a
     * readable string rather than the default memory address of this instantiated object.
     *
     * Currently used for displaying list in {@link fragment.dialog.DialogInput}
     *
     * @return is of type {@link String} and is the type of this instantiated object.
     */
    @Override
    public String toString() {return mType;}

    /**
     * Parcelable.Creator object, as required by {@link Parcelable}
     */
    public static final Parcelable.Creator<Type> CREATOR = new Parcelable.Creator<Type>() {
        //Fires when instance of this class is recreated from a Parcel
        @Override
        public Type createFromParcel(Parcel in) {return new Type(in);}

        //Fires when instance of an array of this class is recreated from a Parcel
        @Override
        public Type[] newArray(int size) {return new Type[size];}
    };

    /**
     * Constructor used by {@link Parcelable}
     * @param in is of type {@link Parcel} and is the flattened version on this object.
     */
    private Type(Parcel in) {
        setId(in.readLong());
        setType(in.readString());
    }

    /**
     * Returns any special flags describing the contents.
     * Returns zero if there's nothing to declare.
     *
     * @return is of primitive type int and is/are content descriptor flag(s).
     */
    @Override
    public int describeContents() {return 0;}

    /**
     * Writes data into {@link Parcel} when called upon to do so.
     *
     * @param out is of type {@link Parcel} and is the flattened data of this class
     * @param flags is of primitive type int and are flags about how data should be written.
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(getId());
        out.writeString(getType());
    }
}