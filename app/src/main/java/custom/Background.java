package custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.zenoyuki.notr.R;

/**
 * Created by Zeno Yuki on 4/19/18.
 *
 * Provides a background to the notes list.
 *
 * This view draws a solid background color before drawing a pattern provided by repeating
 * a {@link BitmapDrawable} with a call to {@link BitmapDrawable#setTileModeXY(Shader.TileMode, Shader.TileMode)}.
 */
public class Background extends View implements ViewSupport.ViewStructure {
    //Object used for drawing pattern
    private BitmapDrawable mTile;
    //Object used to draw background color
    private Rect mTileRect;

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     */
    public Background(Context context) {
        super(context);
        initialize(null);
    }

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     */
    public Background(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    /**
     * Constructor. Calls super.
     *
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     * @param defStyleAttr An attribute in the current theme that contains a reference to a style
     *                 resource that supplies default values for the view. Can be 0 to not look for defaults.
     */
    public Background(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    /**
     * Constructor. Calls super.
     *
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     * @param defStyleAttr An attribute in the current theme that contains a reference to a style
     *                 resource that supplies default values for the view. Can be 0 to not look for defaults.
     * @param defStyleRes A resource identifier of a style resource that supplies default values for
     *                    the view, used only if defStyleAttr is 0 or can not be found in the theme.
     *                    Can be 0 to not look for defaults.
     */
    public Background(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    /**
     * Initialize objects required for this custom view. Is called at every constructor.
     * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
     */
    @Override
    public void initialize(@Nullable AttributeSet attrs) {
        Bitmap bm = ViewSupport.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_vector_bg_item);
        mTile = new BitmapDrawable(getResources(), bm);
    }

    /**
     * Fires when parent requires view to measure itself.
     *
     * @param widthMeasureSpec is of primitive type int and is the width measure spec, provided by
     *                         the parent.
     * @param heightMeasureSpec is of primitive type int and is the height measure spec, provided
     *                          by the parent.
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int resolvedW = resolveSize(getDesiredWidth(), widthMeasureSpec);
        final int resolvedH = resolveSize(getDesiredHeight(), heightMeasureSpec);

        //Calls super's setMeasuredDimension as required by this method's super's contract.
        super.setMeasuredDimension(resolvedW, resolvedH);
    }

    /**
     * If old measures are different to new measures, fire {@link #updateContentBounds(int, int)}
     *
     * @param w is of primitive type int and is the new width of the view
     * @param h is of primitive type int and is the new height of the view
     * @param oldw is of primitive type int and is the old width of the view
     * @param oldh is of primitive type int and is the old height of the view
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(w != oldw || h != oldh) updateContentBounds(w, h);
    }

    /**
     * Retrieves view's desired width.
     * @return is of primitive type int and is this view's desired width.
     */
    @Override
    public int getDesiredWidth() {return ViewSupport.getScreenWidth();}

    /**
     * Retrieves view's desired height.
     * @return is of primitive type int and is this view's desired height.
     */
    @Override
    public int getDesiredHeight() {return ViewSupport.getScreenHeight();}

    /**
     * Fires when view is requested to draw.
     * @param canvas is of type {@link Canvas} and is the area in which this view will draw in.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        mTile.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        mTile.setBounds(mTileRect);
        mTile.draw(canvas);
    }

    /**
     * Updates view content bounds.
     * @param availableW is of primitive type int and is the amount of width available for
     *                   elements to be placed in.
     * @param availableH is of primitive type int and is the amount of height available for
     */
    @Override
    public void updateContentBounds(int availableW, int availableH) {
        if(mTileRect == null) mTileRect = new Rect();
        mTileRect.set(0, 0, availableW, availableH);
    }
}
