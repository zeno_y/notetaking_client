package custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Zeno Yuki on 5/16/18.
 *
 * A {@link RecyclerView} that holds reference to an view to display when the recycler adapter is
 * empty. This is stock behavior for {@link android.widget.ListView} and so this class attempts to
 * maintain consistency.
 */
public class RecyclerViewEmptySupport extends RecyclerView {
    //Empty view to display when adapter is empty
    private View mEmptyView;

    //Listener for adapter data change.
    private AdapterDataObserver mObserver = new AdapterDataObserver() {
        /**
         * Fires when data changes within the adapter.
         */
        @Override
        public void onChanged() {
            Adapter<?> adapter = getAdapter();

            if(adapter != null) {
                final boolean isEmpty = adapter.getItemCount() < 1;
                //Toggles empty View and RecyclerView visibility accordingly.
                if(mEmptyView != null) mEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
                RecyclerViewEmptySupport.this.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
            }
        }
    };

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     */
    public RecyclerViewEmptySupport(Context context) {super(context);}

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     */
    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructor. Calls super.
     *
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     * @param defStyle An attribute in the current theme that contains a reference to a style
     *                 resource that supplies default values for the view. Can be 0 to not look for defaults.
     */
    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Stores reference to an empty view and calls the listener's
     * {@link AdapterDataObserver#onChanged()} method.
     *
     * @param v is of type {@link View} and is a view to display when Recyclerview is empty.
     */
    public void setEmptyView(View v) {
        mEmptyView = v;
        mObserver.onChanged();
    }

    /**
     * Removes listener from adapter
     */
    public void unregisterObserver() {
        Adapter<?> adapter = getAdapter();
        if(mObserver == null || adapter == null) return;

        adapter.unregisterAdapterDataObserver(mObserver);
    }

    /**
     * Sets Recyclerview adapter by calling super, but after attaching a listener to said adapter.
     * @param adapter is of type {@link android.widget.Adapter} and is the adapter that holds data
     *                for this RecyclerView.
     */
    @Override
    public void setAdapter(Adapter adapter) {
        adapter.registerAdapterDataObserver(mObserver);
        super.setAdapter(adapter);
    }
}
