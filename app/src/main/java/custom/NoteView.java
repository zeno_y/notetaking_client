package custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.v4.content.ContextCompat;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.zenoyuki.notr.R;

/**
 * Created by Zeno Yuki on 4/17/18.
 *
 * Provides a card-like view (with shadow and rounded edges) for displaying {@link model.Note} data.
 * Card background color will change depending on what kind of note it is displaying.
 */
public class NoteView extends View implements ViewSupport.ViewStructure {
    private static final int BG_RECT_RAD = 12;
    private static final float TEXT_SKEW = -0.25f;

    //Objects used to store/position/draw background data
    private Rect mBgRect;
    private Paint mBgPaint;

    //Objects used to store/position/draw date text
    private CharSequence mDate;
    @Nullable private StaticLayout mDateLayout;
    private TextPaint mDatePaint;
    private Point mDateOrigin;

    //Objects used to store/position/draw note text
    private CharSequence mNote;
    @Nullable private StaticLayout mNoteLayout;
    private TextPaint mNotePaint;
    private Point mNoteOrigin;

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     */
    public NoteView(Context context) {
        super(context);
        initialize(null);
    }

    /**
     * Constructor. Calls super.
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     */
    public NoteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    /**
     * Constructor. Calls super.
     *
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     * @param defStyleAttr An attribute in the current theme that contains a reference to a style
     *                 resource that supplies default values for the view. Can be 0 to not look for defaults.
     */
    public NoteView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    /**
     * Constructor. Calls super.
     *
     * @param context is of type {@link Context} and is the context of the calling class.
     * @param attrs is of type {@link AttributeSet} and is a set of attributes applied to this View.
     * @param defStyleAttr An attribute in the current theme that contains a reference to a style
     *                 resource that supplies default values for the view. Can be 0 to not look for defaults.
     * @param defStyleRes A resource identifier of a style resource that supplies default values for
     *                    the view, used only if defStyleAttr is 0 or can not be found in the theme.
     *                    Can be 0 to not look for defaults.
     */
    public NoteView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(attrs);
    }

    /**
     * Initialize objects required for this custom view. Is called at every constructor.
     * @param attrs Nullable object {@link AttributeSet} passed on from constructor param
     */
    @Override
    public void initialize(@Nullable AttributeSet attrs) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        //Sets up default values. Custom attrs can use this when implemented.
        @ColorRes int bgColor = R.color.color_tertiary;
        @DimenRes int textSize = R.dimen.textsize_default;
        @ColorRes int dateColor = R.color.textcolor_secondary;
        @ColorRes int noteColor = R.color.textcolor_primary;

        if(attrs != null) {

        }

        mBgPaint = ViewSupport.getPaintById(getContext(), bgColor);
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setShadowLayer(
                4f, 1f, 3f, ContextCompat.getColor(getContext(), R.color.color_shadow)
        );

        mDatePaint = ViewSupport.getTextPaintObj(getContext(), textSize, dateColor);
        mDatePaint.setTextSkewX(TEXT_SKEW); //Makes text italic

        mNotePaint = ViewSupport.getTextPaintObj(getContext(), textSize, noteColor);

        mDate = getResources().getString(R.string.note_default_date);
        mNote = getResources().getString(R.string.note_default_note);
    }

    /**
     * Sets date and note text of this view. Requests re-layout if possible.
     *
     * @param date is of type {@link CharSequence} and is the date text for this note.
     * @param note is of type {@link CharSequence} and is the text for this note.
     */
    public void setText(CharSequence date, CharSequence note) {
        mDate = date;
        mNote = note;

        if(!isInLayout()) requestLayout();
    }

    /**
     * Sets color of note background. Invalidates view.
     * @param id is of primitive type int and is the color resource ID of the color to use.
     */
    public void setColor(@ColorRes int id) {
        mBgPaint.setColor(ContextCompat.getColor(getContext(), id));
        invalidate();
    }

    /**
     * Fires when parent requires view to measure itself. {@link StaticLayout} for texts are
     * processed here, to take advantage of getting raw width data.
     *
     * @param widthMeasureSpec is of primitive type int and is the width measure spec, provided by
     *                         the parent.
     * @param heightMeasureSpec is of primitive type int and is the height measure spec, provided
     *                          by the parent.
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int resolvedW = resolveSize(getDesiredWidth(), widthMeasureSpec);
        final int availableTextW = resolvedW - getPaddingStart() - getPaddingEnd();

        mDateLayout = ViewSupport.getTextLayout(mDate, mDatePaint, availableTextW);
        mNoteLayout = ViewSupport.getTextLayout(mNote, mNotePaint, availableTextW);

        final int resolvedH = resolveSize(getDesiredHeight(), heightMeasureSpec);

        super.setMeasuredDimension(resolvedW, resolvedH);
    }

    /**
     * If old measures are different to new measures, fire {@link #updateContentBounds(int, int)}
     *
     * @param w is of primitive type int and is the new width of the view
     * @param h is of primitive type int and is the new height of the view
     * @param oldw is of primitive type int and is the old width of the view
     * @param oldh is of primitive type int and is the old height of the view
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if(oldw != w || oldh != h) updateContentBounds(w, h);
    }

    /**
     * Retrieves view's desired width.
     * @return is of primitive type int and is this view's desired width.
     */
    @Override
    public int getDesiredWidth() {return ViewSupport.getScreenWidth();}

    /**
     * Retrieves this view's desired height.
     * This view's height should have the following structure:
     *
     * padding, date, padding, note, padding
     *
     * @return is of primitive type int and is this view's desired height.
     */
    @Override
    public int getDesiredHeight() {
        int h = getPaddingTop();

        h += mDateLayout != null ? mDateLayout.getHeight() : 0;
        h += getTextPadding();

        h += mNoteLayout != null ? mNoteLayout.getHeight() : 0;
        h += getPaddingBottom();

        return h;
    }

    /**
     * Fires when view is requested to draw.
     * @param canvas is of type {@link Canvas} and is the area in which this view will draw in.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        ViewSupport.drawRoundedRect(canvas, mBgRect, mBgPaint, BG_RECT_RAD);
        ViewSupport.drawText(canvas, mDateLayout, mDateOrigin);
        ViewSupport.drawText(canvas, mNoteLayout, mNoteOrigin);
    }

    /**
     * Updates view content bounds.
     * @param availableW is of primitive type int and is the amount of width available for
     *                   elements to be placed in.
     * @param availableH is of primitive type int and is the amount of height available for
     */
    @Override
    public void updateContentBounds(int availableW, int availableH) {
        //Updates background bounds
        if(mBgRect == null) mBgRect = new Rect();
        mBgRect.set(3, 3, availableW - 5, availableH - 5);

        int textX = getPaddingStart();
        int textY = getPaddingTop();

        //Updates date text origin
        if(mDateOrigin == null) mDateOrigin = new Point();
        mDateOrigin.set(textX, textY);

        textY += ViewSupport.getTextLineHeight(mDatePaint) + getTextPadding();

        //Updates note text origin
        if(mNoteOrigin == null) mNoteOrigin = new Point();
        mNoteOrigin.set(textX, textY);
    }

    /**
     * Retrieves text padding size.
     * @return is of primitive type int and is the padding size (in px) for separating text.
     */
    @Px
    private int getTextPadding() {return getResources().getDimensionPixelSize(R.dimen.padding_1);}
}
