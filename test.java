import java.util.ArrayList;
import java.util.List;

class Main {
    private static final String SPACER = " ";
    public static void main(String[] args) {
        List<String> parsedKeys = new ArrayList<>();

        int i = getToParse().indexOf(SPACER);
        int iPrev = 0;
        do {
            if(i - iPrev > 1) {
                final String keyword = getToParse().substring(iPrev > 0 ? iPrev + 1 : 0, i);
                parsedKeys.add(keyword);
            }
            iPrev = i;
            i = getToParse().indexOf(SPACER, iPrev + 1);
        } while(i > 0);

        for(int j = 0; j < parsedKeys.size(); j++) {
            System.out.println(parsedKeys.get(j));
        }

        System.out.println("there are " + parsedKeys.size() + " items");
    }

    private static String getToParse() {
        return "how    does i this String look as a List<String>? 10 ";
    }
}
